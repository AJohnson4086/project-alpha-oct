from django.urls import path
from tasks.views import create_task
from projects.views import task_list


urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", task_list, name="show_my_tasks"),
]
